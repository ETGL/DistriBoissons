Objectifs : 

Projet "Exemple" de l'ETGL, permettant de montrer la mise en oeuvre de différentes méthodes, techniques et outils qui font parties du cursus de l'école : 
Gestion des Exigences, Gestion de version, UML, Java, Tests Unitaires, etc.

Contenu : 

Une simulation logicielle d'un distributeur de boisson
